#include <iostream>
#include <array>
#include <iomanip>
#include <regex>
#include "calendar.hxx"
/*C++ program that prints out a calendar for a given month.

April 2018
Mon Tue Wed Thu Fri Sat Sun
                         1
 2   3   4   5   6   7   8
 9  10  11  12  13  14  15
16  17  18  19  20  21  22
23  24  25  26  27  28  29
30

Command-line arguments:
No program argument: Print calendar for current month
Month Name: Print calendar for given month in current year: -m month name/month number
Month and year: Print calendar for given month in given year: -y year

Start week on Sunday (or any other week-day if you like): -s Sun or -s Mon
Print more than one month, if given an interval as input: -i start month-end month
Set language, such as Swedish week-day and month names.: -l en or se
Help text: -h
 */
using namespace std;

namespace Calend {
    const string argMonth = "-m";
    const string argYear = "-y";
    const string argLanguage = "-l";
    const string argInterval = "-i";
    const string argStartDay = "-s";
    const string argHelp = "-h";

    string year;
    string month;
    string lang;
    string startDay;
}
using namespace Calend;
void validateArgs (int args, char **argv);
bool isValidArgs (int argc, char **argv);
void printHelp ();
void setArgsData(int argc, char **argv);
void setCalendarData(Calendar &calendar);
void printCalendar(Calendar &calendar);

int main (int argc, char **argv) {

    try {
        validateArgs(argc, argv);
        setArgsData(argc, argv);
        Calendar calendar;
        setCalendarData(calendar);
        calendar.populateMonthMap();
        printCalendar(calendar);
    }
    catch (invalid_argument &err) {
        cerr << err.what() << endl;
        printHelp();
    }
    catch (out_of_range &err) {
        cerr << err.what() << endl;
        printHelp();
    }
    catch (exception &exp) {
        cerr << exp.what() << endl;
        printHelp();

    }
    return 0;
}

void validateArgs (int args, char **argv) {
    if (!isValidArgs(args, argv)) throw invalid_argument("Not valid arguments");
}

bool isValidArgs (int argc, char **argv) {
    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg[0] != '-') return false;
        if ((arg[1] == 'm' || arg[1] == 'y' || arg[1] == 'l' || arg[1] == 's') && arg.length() == 2) {
            if (argv[k + 1] == nullptr) return false;
            string arg1 = argv[++k];
            if (arg1.length() == 0 || arg1[0] == '-') {
                return false;
            }
        } else if (arg[1] == 'h') {
            return true;
        } else {
            return false;
        }
    }
    return true;
}
void setArgsData(int argc, char **argv){

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == argMonth) {
            month = argv[++k];
        } else if (arg == argYear) {
            year = argv[++k];
        } else if (arg == argLanguage) {
            lang = argv[++k];
        } else if (arg == argStartDay) {
            startDay = argv[++k];
        } else if (arg == argHelp) {
            printHelp();
            cout<<endl;
        }
    }
}

void setCalendarData(Calendar &calendar){
    if (!month.empty()) {
        calendar.convertMonthStringToMonth(month);
        if (calendar.isMonthInterval)
            validateInterval(month);
    }
    if (!year.empty()) {
        calendar.convertYearStringToYear(year);
        calendar.validateYear();
    }
    if (!lang.empty()) {
        calendar.setLang(lang);
        calendar.validateLanguage();
    }
    if (!startDay.empty()) {
        calendar.setStartDay(startDay);
        calendar.validateStartDay();
    }
}

void printHelp () {
    cout << "Usage:" << endl;
    cout << argMonth << " month       Number or name or from-to" << endl;
    cout << argYear << " year" << endl;
    cout << argStartDay << " start day" << endl;
    cout << argLanguage << " language    en or se" << endl;
    cout << argHelp << "             print help";
}

void printCalendar(Calendar &calendar){
    if (!calendar.isMonthInterval) {
        calendar.printMonth(calendar.getYear(), calendar.getMonth(), calendar.getLang(), calendar.getStartDay());
    } else {
        try {
            unsigned short intFirst = convertIntervalToInts(month).first;
            unsigned short intSecond = convertIntervalToInts(month).second;

            for (unsigned short i = intFirst; i <= intSecond; i++) {
                calendar.printMonth(calendar.getYear(), i, calendar.getLang(), calendar.getStartDay());
                cout << endl;
            }
        } catch (invalid_argument &err) {
            throw err;
        }
    }
}



